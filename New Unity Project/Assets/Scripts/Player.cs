﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Player : MonoBehaviour {
	public Animator animator;
	public float speed = 6.0F;
	public float jumpSpeed = 8.0F;
	public float gravity = 20.0F;
	private Vector3 moveDirection = Vector3.zero;
	
	private void OnTriggerEnter(Collider objectPlayerCollidedWith)
	{
		if (objectPlayerCollidedWith.tag == "coin") 
		{
			objectPlayerCollidedWith.gameObject.SetActive(false);
		}
	}

	void Start () 
	{
		animator = GetComponent<Animator>();
	}


	void Update () {
		Vector3 pos = transform.position;
		pos.z = 0;
		transform.position = pos;
		if((Input.GetAxis("Horizontal")<-100)){
			animator.SetTrigger("Player Walk");}
		CharacterController controller = GetComponent<CharacterController>();
		if (controller.isGrounded) {
			moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
			moveDirection = transform.TransformDirection(moveDirection);
			moveDirection *= speed;
			if (Input.GetButton("Jump"))
				moveDirection.y = jumpSpeed;
			
		}
		moveDirection.y -= gravity * Time.deltaTime;
		controller.Move(moveDirection * Time.deltaTime);
	}
}