﻿using UnityEngine;
using System.Collections;

public class CoinPickup : MonoBehaviour {

	void OnTriggerEnter2D (Collider2D col)
	{
		if (col.gameObject.tag == "coin") {
			Destroy (col.gameObject);
		}
}
}
